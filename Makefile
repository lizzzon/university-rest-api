WEB_CONTAINER_NAME=university_web

mkm: ## make migrations
	docker exec -it ${WEB_CONTAINER_NAME} python manage.py makemigrations

m: ## migrate
	docker exec -it ${WEB_CONTAINER_NAME} python manage.py migrate

shell: ## shell
	docker exec -it ${WEB_CONTAINER_NAME} python manage.py shell

csu: ## create superuser
	docker exec -it ${WEB_CONTAINER_NAME} python manage.py createsuperuser

pr:  ## run pre-commit
	pre-commit run --all-files

translate:
# 	docker exec -it ${WEB_CONTAINER_NAME} python ./manage.py makemessages -l ru
	docker exec -it ${WEB_CONTAINER_NAME} python ./manage.py compilemessages --use-fuzzy

rm_m:
	docker exec -it ${WEB_CONTAINER_NAME} find ./apps/authentication/ -path "*/migrations/*" -not -name "__init__.py" -delete
	docker exec -it ${WEB_CONTAINER_NAME} find ./apps/university/ -path "*/migrations/*" -not -name "__init__.py" -delete
