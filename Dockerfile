FROM python:3.11-slim as base

ENV PYTHONUNBUFFERED=1
ENV PYTHONDONTWRITEBYTECODE=1

RUN apt-get update \
    && apt-get install -y curl

RUN apt install gettext -y

# Stage 2: Install Poetry
FROM base as poetry

ENV POETRY_VERSION=1.3.1
ENV POETRY_HOME=/opt/poetry
ENV PATH="$POETRY_HOME/bin:$PATH"

RUN curl -sSL https://install.python-poetry.org | python3 -
WORKDIR /app
COPY pyproject.toml poetry.lock ./

RUN poetry config virtualenvs.create false
RUN poetry lock
RUN poetry install

# Stage 3: Export requirements
FROM poetry as requirements-builder

COPY pyproject.toml poetry.lock ./
RUN poetry export --format requirements.txt --output requirements.txt

# Stage 4: Final image
FROM base as final
WORKDIR /app
COPY --from=requirements-builder /app/requirements.txt /app
RUN pip install --require-hashes --no-cache-dir -r requirements.txt
COPY ./src /app
CMD [ "python", "./manage.py", "runserver"]
