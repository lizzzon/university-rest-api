from rest_framework import filters
from rest_framework.viewsets import ModelViewSet, ReadOnlyModelViewSet


class CoreViewSet:
    filter_backends = (filters.SearchFilter,)
    search_fields = []
    action_serializers = {"retrieve": None, "list": None}

    def get_serializer_class(self):
        if self.action in self.action_serializers:
            return self.action_serializers[self.action]
        return super().get_serializer_class()

    def get_permissions(self):
        try:
            return [
                permission() for permission in self.permission_by_action[self.action]
            ]
        except (KeyError, AttributeError):
            return [permission() for permission in self.permission_classes]


class AbsReadOnlyModelViewSet(CoreViewSet, ReadOnlyModelViewSet):
    pass


class AbsCRUDView(CoreViewSet, ModelViewSet):
    pass
