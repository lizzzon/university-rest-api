from rest_framework import pagination
from rest_framework.response import Response

from core.conf import settings


class CustomPagination(pagination.PageNumberPagination):
    page_size_query_param = "page_size"

    def get_page_size(self, request):
        page_size = request.query_params.get(self.page_size_query_param)
        if page_size is None:
            return settings.REST_FRAMEWORK["PAGE_SIZE"]
        elif page_size == "-1":
            return None
        return super().get_page_size(request=request)

    def get_paginated_response(self, data):
        return Response(
            {
                "next": self.get_next_link(),
                "previous": self.get_previous_link(),
                "page_size": self.page.paginator.per_page,
                "results": data,
            }
        )
