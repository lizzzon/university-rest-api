��    L      |  e   �      p  #   q     �     �     �     �     �     �     �     �     �               /     E     [     l     �     �     �     �     �     �     �     �     �  D   	  7   N  
   �     �     �  	   �     �  	   �     �     �     �     �  	   �     �     	     	     	     !	     &	     ,	     1	     9	     L	  
   Y	     d	     t	     |	     �	     �	     �	     �	     �	     �	     �	     �	     �	  -   �	  /   
  @   N
  /   �
     �
     �
     �
  
   �
     �
     �
     �
     �
     �
       �    A   �     )  
   B     M     h     �     �  )   �     �     �  8        P     n  6   �     �     �  4   �  B   ,  #   o  !   �  @   �  
   �               (  s   F  f   �     !     0     P     _     t     �     �  %   �     �     �     �                4     C     R     c     r     {  $   �     �     �  +   �                @     X     i  !   x     �     �     �  !   �       :     E   X  �   �  `        �     �     �     �     �     �               /     B                  >       ?      &       1   (   )   -   A              9      B      0   .   ;   %   L   
   I   $      :       3      !                *   =         ,   2          7       J                     	                 #           5                            F   <   4       K   E              C   H   "   6             +          /   '          D   @   G   8       Account activation on %(site_name)s Activate Address Administration Administrations Auditory Authentication Can add User Can add group Can add permission Can add speciality Can change group Can change permission Can change speciality Can delete group Can delete permission Can delete speciality Can view User list Can view group Can view permission Can view speciality City Country Date created Date updated Dear %(first_name)s, you have made a request to reset your password. Dear %(first_name)s, you need to activate your account. Department Department name Departments Employees English Faculties Faculty Faculty name Group number Groups Is active Is staff Is superuser Lesson Lessons Name Phone Role Russian Set a new password Specialities Speciality Speciality name Student Student group Student name Students Subject Subject name Subjects Success in studies! Teacher Teacher name Teachers To activate your profile, click on the button To activate your profile, follow the link below To create a new password, click on the 'Set new password' button To create a new password, follow the link below Union Unions Universities University User Username Users Visit Visits You are not teacher! Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);
 Активация аккаунта на сайте %(site_name)s Активировать Адрес Администрация Администрации Аудитория Аутентификация Добавить пользователя Добавить группу Добавить права Может добавлять специальность Изменить группу Изменить права Может изменять специальность Удалить группу Удалить права Может удалять специальность Просматривать список пользователей Просмотреть группу Просмотреть права Может просматривать специальность Город Страна Дата создания Дата обновления Уважаемый %(first_name)s, вы сделали запрос на восстановление пароля. Уважаемый %(first_name)s, вы должны активировать свой аккаунт. Кафедра Название кафедры Кафедры Сотрудники Английский Факультеты Факультет Название факультета Номер группы Группы Активный? Работник? Суперюзер? Занятие Занятия Название Телефон Роль Русский Задать новый пароль Специальности Специальность Название специальности Студент Группа студентов Имя студента Студенты Предмет Название предмета Предметы Удачи в учебе! Преподаватель Имя преподавателя Преподаватели Для активации нажмите на кнопку Для активации следуйте по ссылке ниже Чтобы создать новый пароль, нажмите кнопку «Установить новый пароль». Чтобы создать новый пароль, перейдите по ссылке ниже Группа Группы Университеты Университет Пользователь Имя пользователя Пользователи Посещение Посещения Вы не учитель! 