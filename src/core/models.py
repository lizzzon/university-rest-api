from django.db import models
from django.utils.translation import gettext_lazy as _


class BaseModel(models.Model):
    date_created = models.DateTimeField(
        verbose_name=_("Date created"), auto_now_add=True, db_comment="date_created"
    )
    date_updated = models.DateTimeField(
        verbose_name=_("Date updated"), auto_now=True, db_comment="date_updated"
    )

    class Meta:
        abstract = True
