from datetime import timedelta

from core.env import env

DJOSER = {
    "PASSWORD_RESET_CONFIRM_URL": "password-reset?uid={uid}&jwt={token}",
    "ACTIVATION_URL": "auth/user-activation?uid={uid}&jwt={token}",
    "SEND_ACTIVATION_EMAIL": True,
    "SERIALIZERS": {
        "user_create": "apps.authentication.serializers.user.UserCreateSerializer"
    },
    "EMAIL": {
        "activation": "apps.authentication.email.ActivateEmail",
        "password_reset": "apps.authentication.email.ResetEmail",
    },
}

SIMPLE_JWT = {
    "ACCESS_TOKEN_LIFETIME": timedelta(days=env.int("TIME_ACCESS")),
    "REFRESH_TOKEN_LIFETIME": timedelta(days=env.int("TIME_REFRESH")),
    "ROTATE_REFRESH_TOKENS": False,
    "BLACKLIST_AFTER_ROTATION": True,
    "AUTH_HEADER_TYPES": ("JWT",),
    "USER_ID_FIELD": "id",
    "USER_ID_CLAIM": "user_id",
    "AUTH_TOKEN_CLASSES": ("rest_framework_simplejwt.tokens.AccessToken",),
    "TOKEN_TYPE_CLAIM": "token_type",
}
