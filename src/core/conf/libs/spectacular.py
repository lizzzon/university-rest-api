SPECTACULAR_SETTINGS = {
    "TITLE": "University",
    "DESCRIPTION": "Администрирование сервиса University",
    "VERSION": "0.0.1",
    "SERVE_INCLUDE_SCHEMA": False,
    "SWAGGER_UI_SETTINGS": {
        "deepLinking": True,
        "persistAuthorization": True,
        "displayRequestDuration": True,
        "filter": True,
        "displayOperationId": True,
    },
    "COMPONENT_SPLIT_REQUEST": True,
}
