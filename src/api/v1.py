from django.urls import include, path

urlpatterns = [
    path("user/", include("apps.authentication.urls")),
    path("university/", include("apps.university.urls")),
]
