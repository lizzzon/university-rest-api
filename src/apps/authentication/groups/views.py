from django.contrib.auth.models import Group, Permission
from drf_spectacular.utils import extend_schema
from rest_framework import filters
from rest_framework.mixins import ListModelMixin
from rest_framework.viewsets import GenericViewSet, ModelViewSet

from apps.authentication.groups.serializers import GroupSerializer, PermissionSerializer
from apps.authentication.permissions import GroupPermissions, PermissionsForPermissions
from core.utils.mixins import PermissionActionMixin, SerializerActionMixin
from core.utils.permission_util import CheckPermissions


@extend_schema(tags=["roles: permissions"])
class PermissionViewSet(
    SerializerActionMixin, PermissionActionMixin, ListModelMixin, GenericViewSet
):
    def_code = [
        "view_session",
        "delete_session",
        "change_session",
        "add_session",
        "view_contenttype",
        "delete_contenttype",
        "change_contenttype",
        "add_contenttype",
        "view_logentry",
        "delete_logentry",
        "change_logentry",
        "add_logentry",
        "add_profile",
        "change_profile",
        "delete_profile",
        "view_profile",
        "view_tokenproxy",
        "add_tokenproxy",
        "change_tokenproxy",
        "delete_tokenproxy",
        "add_token",
        "change_token",
        "delete_token",
        "view_token",
    ]

    queryset = Permission.objects.exclude(codename__in=def_code).prefetch_related(
        "user_set"
    )
    serializer_class = PermissionSerializer
    permission_by_action = {
        "list": (CheckPermissions(PermissionsForPermissions.view_permission),)
    }
    search_fields = ["name", "codename"]
    filter_backends = (filters.SearchFilter, filters.OrderingFilter)
    ordering_fields = ("id",)
    ordering = ("-id",)


@extend_schema(tags=["roles: groups"])
class GroupViewSet(ModelViewSet):
    queryset = Group.objects.all()
    serializer_class = GroupSerializer
    permission_by_action = {
        "list": (CheckPermissions(GroupPermissions.view_group),),
        "create": (CheckPermissions(GroupPermissions.add_group),),
        "put": (CheckPermissions(GroupPermissions.change_group),),
        "patch": (CheckPermissions(GroupPermissions.change_group),),
        "delete": (CheckPermissions(GroupPermissions.delete_group),),
    }
    http_method_names = ["get", "post", "put", "delete"]
