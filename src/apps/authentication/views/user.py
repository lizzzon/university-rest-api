from djoser.views import UserViewSet
from drf_spectacular.utils import extend_schema
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
    TokenVerifyView,
)

from apps.authentication.models import User
from apps.authentication.permissions import UserPermissions
from apps.authentication.serializers.user import UserCreateSerializer, UserSerializer
from core.utils.permission_util import CheckPermissions
from core.views import AbsReadOnlyModelViewSet


@extend_schema(tags=["auth: user"])
class UserAuthViewSet(AbsReadOnlyModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    action_serializers = {
        "create": UserCreateSerializer,
    }
    permission_by_action = {
        "list": (CheckPermissions(UserPermissions.view_user),),
        "retrieve": (CheckPermissions(UserPermissions.view_user),),
        "create": (CheckPermissions(UserPermissions.create_user),),
    }

    @action(detail=False, methods=["GET"])
    def me(self, request):
        serializer = self.get_serializer(request.user)
        return Response(serializer.data)


@extend_schema(tags=["auth: user"])
class CreateTokenView(TokenObtainPairView):
    pass


@extend_schema(tags=["auth: user"])
class RefreshTokenView(TokenRefreshView):
    pass


@extend_schema(tags=["auth: user"])
class VerifyTokenView(TokenVerifyView):
    pass


@extend_schema(tags=["auth: user"])
class DjoserUserViewSet(UserViewSet):
    pass
