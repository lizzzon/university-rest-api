import phonenumbers
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _
from phonenumbers import NumberParseException
from rest_framework import serializers


def name_validator(value):
    if not value.isalpha():
        raise ValidationError(_("Only letters are allowed."))


class PhoneNumberValidator:
    def __call__(self, value):
        try:
            parsed_number = phonenumbers.parse(value, None)
            if not phonenumbers.is_valid_number(parsed_number):
                raise serializers.ValidationError(_("Enter correct phone number"))
        except NumberParseException:
            raise serializers.ValidationError(_("Enter correct phone number"))
