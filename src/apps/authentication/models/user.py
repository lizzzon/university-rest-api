from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.contrib.auth.password_validation import validate_password
from django.core.validators import MinLengthValidator
from django.db import models
from django.db.models import TextChoices
from django.utils.translation import gettext_lazy as _

from apps.authentication.models.managers import UserManager
from apps.authentication.permissions import UserPermissions
from apps.authentication.validators import name_validator


class User(AbstractBaseUser, PermissionsMixin):
    class Role(TextChoices):
        STUDENT = "student"
        TEACHER = "teacher"

    username = models.EmailField(verbose_name=_("Username"), unique=True)
    first_name = models.CharField(
        verbose_name=_("First name"),
        max_length=128,
        validators=[name_validator],
        default="test",
    )
    last_name = models.CharField(
        verbose_name=_("Last name"),
        max_length=128,
        validators=[name_validator],
        default="test",
    )
    is_superuser = models.BooleanField(verbose_name=_("Is superuser"), default=False)
    is_staff = models.BooleanField(verbose_name=_("Is staff"), default=False)
    is_active = models.BooleanField(verbose_name=_("Is active"), default=True)
    role = models.CharField(verbose_name=_("Role"), max_length=32, choices=Role.choices)
    password = models.CharField(
        max_length=255, validators=[validate_password, MinLengthValidator(8)]
    )

    USERNAME_FIELD = "username"
    objects = UserManager()
    EMAIL_FIELD = "username"

    class Meta:
        db_table = "users"
        verbose_name = _("User")
        verbose_name_plural = _("Users")
        default_permissions = []
        permissions = UserPermissions.choices
