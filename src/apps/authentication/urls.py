from django.urls import path
from rest_framework.routers import SimpleRouter

from apps.authentication.groups.views import GroupViewSet, PermissionViewSet
from apps.authentication.views.user import (
    CreateTokenView,
    DjoserUserViewSet,
    RefreshTokenView,
    UserAuthViewSet,
    VerifyTokenView,
)

router = SimpleRouter()
router.register("user", UserAuthViewSet)
router.register("permissions", PermissionViewSet)
router.register("groups", GroupViewSet)

urlpatterns = [
    path("jwt/create/", CreateTokenView.as_view(), name="jwt-create"),
    path("jwt/refresh/", RefreshTokenView.as_view(), name="jwt-refresh"),
    path("jwt/verify/", VerifyTokenView.as_view(), name="jwt-verify"),
    path(
        "reset-password/",
        DjoserUserViewSet.as_view({"post": "reset_password"}),
        name="user-reset-password",
    ),
    path(
        "reset-password-confirm/",
        DjoserUserViewSet.as_view({"post": "reset_password_confirm"}),
        name="reset-password-confirm",
    ),
    path(
        "user-create/",
        DjoserUserViewSet.as_view({"post": "create"}),
        name="user-create",
    ),
    *router.urls,
]
