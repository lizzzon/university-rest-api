from djoser.email import ActivationEmail, PasswordResetEmail


class ActivateEmail(ActivationEmail):
    template_name = "activation.html"

    def get_context_data(self):
        context = super().get_context_data()
        user = context.get("user")
        if user:
            context["first_name"] = user.first_name
        return context


class ResetEmail(PasswordResetEmail):
    template_name = "reset_password.html"

    def get_context_data(self):
        context = super().get_context_data()
        user = context.get("user")
        if user:
            context["first_name"] = user.first_name
        return context
