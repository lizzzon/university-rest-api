from django.urls import path
from rest_framework.routers import SimpleRouter

from apps.university.views.lesson import (
    StudentLessonsTodayView,
    StudentLessonsWeekendView,
)

# from apps.university.views.speciality import SpecialityViewSet
from apps.university.views.student import StudentInMyGroupView, StudentsView
from apps.university.views.teacher import TeachersView

router = SimpleRouter()
# router.register("speciality", SpecialityViewSet)


urlpatterns = [
    path(
        "student/lessons-today/",
        StudentLessonsTodayView.as_view(),
        name="student_lessons_today",
    ),
    path(
        "student/lessons-weekend/",
        StudentLessonsWeekendView.as_view(),
        name="student_lessons_weekend",
    ),
    path(
        "student/my-group/",
        StudentInMyGroupView.as_view(),
        name="my_group",
    ),
    path(
        "teacher/all",
        TeachersView.as_view(),
        name="teachers",
    ),
    path(
        "students/all",
        StudentsView.as_view(),
        name="students",
    ),
    *router.urls,
]
