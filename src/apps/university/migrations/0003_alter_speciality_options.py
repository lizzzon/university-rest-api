# Generated by Django 4.2.5 on 2023-11-30 10:29

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("university", "0002_alter_student_phone_alter_teacher_phone"),
    ]

    operations = [
        migrations.AlterModelOptions(
            name="speciality",
            options={
                "default_permissions": [],
                "permissions": [],
                "verbose_name": "Speciality",
                "verbose_name_plural": "Specialities",
            },
        ),
    ]
