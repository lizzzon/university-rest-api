from rest_framework import serializers

from apps.university.models import Student


class StudentSerializer(serializers.ModelSerializer):
    first_name = serializers.SerializerMethodField()
    last_name = serializers.SerializerMethodField()

    class Meta:
        model = Student
        fields = (
            "first_name",
            "last_name",
            "phone",
            "address",
        )

    def get_first_name(self, obj):
        return obj.user.first_name if obj.user else None

    def get_last_name(self, obj):
        return obj.user.last_name if obj.user else None
