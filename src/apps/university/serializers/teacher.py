from rest_framework import serializers

from apps.university.models import Teacher


class TeacherSerializer(serializers.ModelSerializer):
    first_name = serializers.SerializerMethodField()
    last_name = serializers.SerializerMethodField()
    subject = serializers.SerializerMethodField()

    class Meta:
        model = Teacher
        fields = (
            "first_name",
            "last_name",
            "phone",
            "subject",
        )

    def get_first_name(self, obj):
        return obj.user.first_name if obj.user else None

    def get_last_name(self, obj):
        return obj.user.last_name if obj.user else None

    def get_subject(self, obj):
        return obj.subject.name if obj.subject else None
