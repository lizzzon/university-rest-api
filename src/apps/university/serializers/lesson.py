from rest_framework import serializers

from apps.university.models import Lesson


class LessonSerializer(serializers.ModelSerializer):
    subject_name = serializers.SerializerMethodField()
    teacher_first_name = serializers.SerializerMethodField()
    teacher_last_name = serializers.SerializerMethodField()
    groups_number = serializers.SerializerMethodField()

    class Meta:
        model = Lesson
        fields = (
            "auditory",
            "time",
            "date",
            "groups_number",
            "subject_name",
            "teacher_first_name",
            "teacher_last_name",
        )

    def get_subject_name(self, obj):
        return obj.subject.name if obj.subject else None

    def get_teacher_first_name(self, obj):
        return obj.teacher.user.first_name if obj.teacher and obj.teacher.user else None

    def get_teacher_last_name(self, obj):
        return obj.teacher.user.last_name if obj.teacher and obj.teacher.user else None

    def get_groups_number(self, obj):
        return obj.groups.number if obj.groups else None
