from rest_framework import serializers

from apps.university.models import Union
from apps.university.serializers.students import StudentSerializer


class AddGroupsToSpecialitySerializer(serializers.Serializer):
    groups = serializers.PrimaryKeyRelatedField(
        queryset=Union.objects.all(),
        many=True,
    )


class SpecialityGroupSerializer(serializers.ModelSerializer):
    students = StudentSerializer(many=True)

    class Meta:
        model = Union
        fields = (
            "number",
            "students",
        )
