# from drf_writable_nested import WritableNestedModelSerializer
#
# from apps.university.models import Speciality
# from apps.university.serializers.union import SpecialityGroupSerializer
#
#
# class SpecialitySerializer(WritableNestedModelSerializer):
#     groups = SpecialityGroupSerializer(many=True)
#
#     class Meta:
#         model = Speciality
#         fields = (
#             "name",
#             "groups",
#         )
