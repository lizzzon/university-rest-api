from django.db import models
from django.utils.translation import gettext_lazy as _


class AdministrationPermissions(models.TextChoices):
    pass


class SpecialityPermissions(models.TextChoices):
    pass


class DepartmentPermissions(models.TextChoices):
    pass


class FacultyPermissions(models.TextChoices):
    pass


class UnionPermissions(models.TextChoices):
    pass


class LessonPermissions(models.TextChoices):
    pass


class SubjectPermissions(models.TextChoices):
    pass


class VisitPermissions(models.TextChoices):
    pass


class StudentPermissions(models.TextChoices):
    pass


class TeacherPermissions(models.TextChoices):
    pass


class UniversityPermissions(models.TextChoices):
    pass
