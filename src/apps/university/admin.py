from django.contrib import admin

from apps.university.models import (
    Administration,
    Department,
    Faculty,
    Lesson,
    Speciality,
    Student,
    Subject,
    Teacher,
    Union,
    Visit,
)


class SpecialityInline(admin.TabularInline):
    model = Speciality
    extra = 0


class StudentInline(admin.TabularInline):
    model = Student
    extra = 0


class TeacherInline(admin.TabularInline):
    model = Teacher
    extra = 0


class UnionInline(admin.TabularInline):
    model = Union
    extra = 0


@admin.register(Administration)
class AdministrationAdmin(admin.ModelAdmin):
    pass


@admin.register(Department)
class DepartmentAdmin(admin.ModelAdmin):
    pass


@admin.register(Lesson)
class LessonAdmin(admin.ModelAdmin):
    pass


@admin.register(Faculty)
class FacultyAdmin(admin.ModelAdmin):
    inlines = [
        SpecialityInline,
    ]


@admin.register(Subject)
class SubjectAdmin(admin.ModelAdmin):
    inlines = [
        TeacherInline,
    ]


@admin.register(Union)
class UnionAdmin(admin.ModelAdmin):
    inlines = [
        StudentInline,
    ]


@admin.register(Visit)
class VisitAdmin(admin.ModelAdmin):
    pass


@admin.register(Speciality)
class SpecialityAdmin(admin.ModelAdmin):
    inlines = [
        UnionInline,
    ]
