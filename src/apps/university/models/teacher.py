from django.db import models
from django.db.models import CASCADE
from django.utils.translation import gettext_lazy as _

from apps.authentication.models import User
from apps.authentication.validators import PhoneNumberValidator
from apps.university.permissions import TeacherPermissions
from core.models import BaseModel


class Teacher(BaseModel):
    user = models.ForeignKey(User, verbose_name=_("User"), on_delete=CASCADE)
    phone = models.CharField(verbose_name=_("Phone"), null=True)
    department = models.ForeignKey(
        to="university.Department",
        verbose_name=_("Employees"),
        default=None,
        on_delete=CASCADE,
    )
    subject = models.ForeignKey(
        to="university.Subject",
        verbose_name=_("Subjects"),
        on_delete=CASCADE,
        default=1,
    )

    def __str__(self):
        return f"{self.user.first_name} {self.user.last_name}"

    class Meta:
        db_table = "teachers"
        verbose_name = _("Teacher")
        verbose_name_plural = _("Teachers")
        default_permissions = []
        permissions = TeacherPermissions.choices
