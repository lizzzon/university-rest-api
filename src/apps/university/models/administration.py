from django.db import models
from django.db.models import CASCADE
from django.utils.translation import gettext_lazy as _

from apps.university.permissions import AdministrationPermissions
from core.models import BaseModel


class Administration(BaseModel):
    name = models.CharField(
        verbose_name=_("Administration"), max_length=100, default=_("Administration")
    )
    department = models.ForeignKey(
        to="university.Department",
        verbose_name=_("Department"),
        on_delete=CASCADE,
        default=None,
    )

    def __str__(self):
        return self.name

    class Meta:
        db_table = "administrations"
        verbose_name = _("Administration")
        verbose_name_plural = _("Administrations")
        default_permissions = []
        permissions = AdministrationPermissions.choices
