from django.db import models
from django.utils.translation import gettext_lazy as _

from apps.university.permissions import FacultyPermissions
from core.models import BaseModel


class Faculty(BaseModel):
    name = models.CharField(
        verbose_name=_("Faculty name"), max_length=100, default=None
    )

    def __str__(self):
        return self.name

    class Meta:
        db_table = "faculties"
        verbose_name = _("Faculty")
        verbose_name_plural = _("Faculties")
        default_permissions = []
        permissions = FacultyPermissions.choices
