from django.db import models
from django.db.models import CASCADE
from django.utils.translation import gettext_lazy as _

from apps.university.permissions import SpecialityPermissions
from core.models import BaseModel


class Speciality(BaseModel):
    faculty = models.ForeignKey(
        to="university.Faculty", verbose_name=_("Faculty"), on_delete=CASCADE
    )
    name = models.CharField(
        verbose_name=_("Speciality name"), max_length=100, unique=True
    )

    def __str__(self):
        return self.name

    class Meta:
        db_table = "specialities"
        verbose_name = _("Speciality")
        verbose_name_plural = _("Specialities")
        default_permissions = []
        permissions = SpecialityPermissions.choices
