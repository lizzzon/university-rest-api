from django.db import models
from django.db.models import CASCADE
from django.utils.translation import gettext_lazy as _

from apps.university.permissions import UnionPermissions
from core.models import BaseModel


class Union(BaseModel):
    number = models.CharField(
        verbose_name=_("Group number"), max_length=10, unique=True
    )
    speciality = models.ForeignKey(
        to="university.Speciality", verbose_name=_("Speciality"), on_delete=CASCADE
    )

    def __str__(self):
        return self.number

    class Meta:
        db_table = "unions"
        verbose_name = _("Union")
        verbose_name_plural = _("Unions")
        default_permissions = []
        permissions = UnionPermissions.choices
