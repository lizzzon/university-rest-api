from django.db import models
from django.db.models import CASCADE
from django.utils.translation import gettext_lazy as _

from apps.university.permissions import LessonPermissions
from core.models import BaseModel


class Lesson(BaseModel):
    groups = models.ForeignKey(
        to="university.Union", verbose_name=_("Groups"), on_delete=CASCADE
    )
    subject = models.ForeignKey(
        to="university.Subject", verbose_name=_("Subjects"), on_delete=CASCADE
    )
    auditory = models.CharField(verbose_name=_("Auditory"), max_length=10, default="")
    teacher = models.OneToOneField(
        to="university.Teacher", verbose_name=_("Teacher"), on_delete=CASCADE
    )
    time = models.TimeField(verbose_name=_("Time"), auto_now=False, default="9:00")
    date = models.DateField(verbose_name=_("Date"))

    def __str__(self):
        return (
            f"предмет: {self.subject.name}, группа: {self.groups.number}, "
            f"{self.auditory}, {self.time}, {self.date}"
        )

    class Meta:
        db_table = "lessons"
        verbose_name = _("Lesson")
        verbose_name_plural = _("Lessons")
        default_permissions = []
        permissions = LessonPermissions.choices
