from django.db import models
from django.db.models import CASCADE
from django.utils.translation import gettext_lazy as _

from apps.university.permissions import VisitPermissions
from core.models import BaseModel


class Visit(BaseModel):
    students = models.ForeignKey(
        to="university.Student", verbose_name=_("Students"), on_delete=CASCADE
    )
    lessons = models.ForeignKey(
        to="university.Lesson", verbose_name=_("Lessons"), on_delete=CASCADE
    )

    def __str__(self):
        return (
            f"визит: {self.students.user.last_name}, группа: {self.lessons.groups.number},"
            f" занятие: {self.lessons.subject.name}, {self.lessons.auditory}, {self.lessons.time}"
        )

    class Meta:
        db_table = "visits"
        verbose_name = _("Visit")
        verbose_name_plural = _("Visits")
        default_permissions = []
        permissions = VisitPermissions.choices
