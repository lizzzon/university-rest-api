from django.db import models
from django.utils.translation import gettext_lazy as _

from apps.university.permissions import DepartmentPermissions
from core.models import BaseModel


class Department(BaseModel):
    name = models.CharField(
        verbose_name=_("Department name"), max_length=100, default=None
    )

    def __str__(self):
        return self.name

    class Meta:
        db_table = "departments"
        verbose_name = _("Department")
        verbose_name_plural = _("Departments")
        default_permissions = []
        permissions = DepartmentPermissions.choices
