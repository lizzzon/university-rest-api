from django.db import models
from django.utils.translation import gettext_lazy as _

from apps.university.permissions import SubjectPermissions
from core.models import BaseModel


class Subject(BaseModel):
    name = models.CharField(verbose_name=_("Subject name"), max_length=100, unique=True)

    def __str__(self):
        return self.name

    class Meta:
        db_table = "subjects"
        verbose_name = _("Subject")
        verbose_name_plural = _("Subjects")
        default_permissions = []
        permissions = SubjectPermissions.choices
