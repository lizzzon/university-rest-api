from django.db import models
from django.db.models import CASCADE
from django.utils.translation import gettext_lazy as _

from apps.authentication.models import User
from apps.authentication.validators import PhoneNumberValidator
from apps.university.permissions import StudentPermissions
from core.models import BaseModel


class Student(BaseModel):
    user = models.ForeignKey(User, verbose_name=_("User"), on_delete=CASCADE)
    group = models.ForeignKey(
        to="university.Union", verbose_name=_("Student group"), on_delete=models.CASCADE
    )
    phone = models.CharField(
        verbose_name=_("Phone"),
        null=True,
    )
    address = models.CharField(verbose_name=_("Address"), null=True)

    def __str__(self):
        return f"{self.user.first_name} {self.user.last_name}"

    class Meta:
        db_table = "students"
        verbose_name = _("Student")
        verbose_name_plural = _("Students")
        default_permissions = []
        permissions = StudentPermissions.choices
