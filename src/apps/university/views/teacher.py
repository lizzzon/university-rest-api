from drf_spectacular.utils import extend_schema
from rest_framework import generics

from apps.university.models import Teacher
from apps.university.serializers.teacher import TeacherSerializer


@extend_schema(tags=["university: teachers"])
class TeachersView(generics.ListAPIView):
    serializer_class = TeacherSerializer

    def get_queryset(self):
        queryset = Teacher.objects.all()
        return queryset
