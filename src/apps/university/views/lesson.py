from datetime import date, timedelta

from django.http import Http404
from drf_spectacular.utils import extend_schema
from rest_framework import generics
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import IsAuthenticated

from apps.authentication.serializers.user import UserSerializer
from apps.university.models import Lesson, Student
from apps.university.serializers.lesson import LessonSerializer
from apps.university.serializers.students import StudentSerializer


@extend_schema(tags=["university: SHEDULE"])
class StudentLessonsTodayView(generics.ListAPIView):
    serializer_class = LessonSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        try:
            student = get_object_or_404(Student, user=self.request.user)
            student_group = student.group
        except Http404:
            student_group = None

        if student_group:
            queryset = Lesson.objects.filter(
                date=date.today(),
                groups=student_group,
            ).distinct()
        else:
            queryset = Lesson.objects.none()

        return queryset


@extend_schema(tags=["university: SHEDULE"])
class StudentLessonsWeekendView(generics.ListAPIView):
    serializer_class = LessonSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        today = date.today()
        week_start = today - timedelta(days=today.weekday())
        week_end = week_start + timedelta(days=6)

        try:
            student = get_object_or_404(Student, user=self.request.user)
            student_group = student.group
        except Http404:
            student_group = None

        if student_group:
            queryset = Lesson.objects.filter(
                date__range=[week_start, week_end],
                groups=student_group,
            ).distinct()
        else:
            queryset = Lesson.objects.none()

        return queryset
