# from drf_spectacular.utils import extend_schema
#
# from apps.university.models import Speciality
# from apps.university.serializers.speciality import SpecialitySerializer
# from core.views import AbsReadOnlyModelViewSet
#
#
# @extend_schema(tags=["university: specialities"])
# class SpecialityViewSet(AbsReadOnlyModelViewSet):
#     queryset = Speciality.objects.all()
#     serializer_class = SpecialitySerializer
