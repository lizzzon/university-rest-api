from django.http import Http404
from django.utils.translation import gettext_lazy as _
from drf_spectacular.utils import extend_schema
from rest_framework import generics, serializers
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import IsAuthenticated

from apps.university.models import Student
from apps.university.serializers.students import StudentSerializer


@extend_schema(tags=["university: My lovely group"])
class StudentInMyGroupView(generics.ListAPIView):
    serializer_class = StudentSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        try:
            student = get_object_or_404(Student, user=self.request.user)
            student_group = student.group
        except Http404:
            student_group = None
        if student_group:
            queryset = Student.objects.filter(
                group=student_group,
            ).distinct()
        else:
            queryset = Student.objects.none()
        return queryset


@extend_schema(tags=["university: students"])
class StudentsView(generics.ListAPIView):
    serializer_class = StudentSerializer

    def get_queryset(self):
        if self.request.user.role == "teacher" or self.request.user.is_superuser:
            queryset = Student.objects.all()
        else:
            raise serializers.ValidationError(
                _("You are not teacher!"),
                code="not_belong_to_teacher",
            )
        return queryset
