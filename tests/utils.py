def has_error(errors: list[dict], code: str, attr) -> bool:
    return any(error["code"] == code and error.get("attr") == attr for error in errors)
