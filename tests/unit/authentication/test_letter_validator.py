import pytest
from django.core.exceptions import ValidationError

from apps.authentication.validators import name_validator


@pytest.mark.parametrize(
    "name, expected_result",
    [
        ("English", None),
        ("12345678y0", ValidationError),
        ("Русский", None),
        ("Polski", None),
        ("12345АB", ValidationError),
        (",@test", ValidationError),
    ],
)
def test_name_validator(name, expected_result):
    validator = name_validator
    if expected_result is None:
        assert validator(name) == expected_result
    else:
        with pytest.raises(expected_result):
            validator(name)
