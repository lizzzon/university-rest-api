import pytest
from rest_framework.exceptions import ValidationError

from apps.authentication.validators import PhoneNumberValidator, name_validator


@pytest.mark.parametrize(
    "phone_number, expected_result",
    [
        ("+375294563745", None),
        ("12345678y0", ValidationError),
        ("+74232499777", None),
        ("+12345", ValidationError),
        ("+12345678901234567890", ValidationError),
        ("abc", ValidationError),
        (None, ValidationError),
    ],
)
def test_phone_validator(phone_number, expected_result):
    validator = PhoneNumberValidator()
    if expected_result is None:
        assert validator(phone_number) == expected_result
    else:
        with pytest.raises(expected_result):
            validator(phone_number)
