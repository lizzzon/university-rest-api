import factory

from apps.authentication.models import User


class UserFactory(factory.Factory):
    class Meta:
        model = User

    username = factory.Sequence(lambda x: f"test_university_user_{x}@gmail.com")
    is_superuser = False
    is_staff = False
    is_active = False
    password = "university_password"
    role = "university"
