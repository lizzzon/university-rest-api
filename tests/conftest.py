import pytest
from rest_framework.test import APIClient

from apps.authentication.models import User
from tests.factory.user_factory import UserFactory

pytestmark = pytest.mark.django_db


@pytest.fixture()
def api_client() -> APIClient:
    return APIClient()


@pytest.fixture()
def create_user(username="test_university_user@gmail.com") -> User:
    user = User.objects.create_user(
        username=username,
        password="university_pass",
        is_superuser=False,
    )
    return user


@pytest.fixture()
def create_superuser(username="admin@gmail.com"):
    user = User.objects.create_superuser(
        username=username,
        password="university_pass",
        is_superuser=True,
    )
    return user


@pytest.fixture()
def create_user_objects(count=20):
    return UserFactory.create_batch(count)
