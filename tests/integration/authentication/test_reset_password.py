import pytest
from django.urls import reverse
from rest_framework import status

pytestmark = pytest.mark.django_db


def get_path() -> str:
    return reverse("user-reset-password")


def test_reset_password__success(api_client, create_user):
    data = {"username": create_user.username}

    response = api_client.post(get_path(), data=data)

    assert response.status_code == status.HTTP_204_NO_CONTENT


def test_reset_password__empty(api_client):
    data = {"username": ""}

    response = api_client.post(get_path(), data=data)

    assert response.status_code == status.HTTP_400_BAD_REQUEST
