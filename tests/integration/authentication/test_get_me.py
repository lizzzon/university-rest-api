import pytest
from django.urls import reverse
from rest_framework import status

pytestmark = pytest.mark.django_db


def get_path() -> str:
    return reverse("user-me")


def test_get_user__success(api_client, create_user):
    api_client.force_authenticate(create_user)

    data = {
        "username": create_user.username,
        "first_name": "university",
        "last_name": "university",
    }

    response = api_client.get(get_path(), data=data)

    assert response.status_code == status.HTTP_200_OK

    assert "username" in response.data
    assert response.data["username"] == create_user.username
    assert "password" not in response.data


def test_get_user__failure(api_client, create_user):
    data = {
        "username": create_user.username,
        "password": "university",
        "first_name": "university",
        "last_name": "university",
    }

    response = api_client.get(get_path(), data=data)

    assert response.status_code == status.HTTP_401_UNAUTHORIZED
