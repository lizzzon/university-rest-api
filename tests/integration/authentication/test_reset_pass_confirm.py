import pytest
from django.contrib.auth.hashers import check_password
from django.contrib.auth.tokens import default_token_generator
from django.urls import reverse
from djoser.utils import encode_uid
from rest_framework import status

from apps.authentication.models import User

pytestmark = pytest.mark.django_db


def get_path() -> str:
    return reverse("reset-password-confirm")


def test_reset_pass_confirm__success(api_client, create_user):
    uid = encode_uid(create_user.pk)
    token = default_token_generator.make_token(create_user)
    new_password = "1384jshfdg"

    response = api_client.post(
        get_path(),
        data={"uid": uid, "token": token, "new_password": new_password},
    )
    user = User.objects.get(username=create_user.username)

    assert response.status_code == status.HTTP_204_NO_CONTENT
    assert check_password(new_password, user.password) is True


def test_reset_pass_confirm__failure(api_client):
    uid = "uid"
    token = "token"
    new_password = "1384jshfdg"

    response = api_client.post(
        get_path(),
        data={"uid": uid, "token": token, "new_password": new_password},
    )

    assert response.status_code == status.HTTP_400_BAD_REQUEST
