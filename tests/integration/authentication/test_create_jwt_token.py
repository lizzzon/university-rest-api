import jwt
import pytest
from django.urls import reverse
from rest_framework import status

from core.env import env

pytestmark = pytest.mark.django_db


def test_create_jwt_token__access(api_client, create_user):
    data = {"username": create_user.username, "password": "university_pass"}
    response = api_client.post(reverse("jwt-create"), data=data)

    assert response.status_code == status.HTTP_200_OK

    access_token = response.json()["access"]
    refresh_token = response.json()["refresh"]

    decoded_access_token = jwt.decode(
        access_token,
        env("DJANGO_SECRET_KEY"),
        algorithms=["HS256"],
        options={"verify_signature": False},
    )
    decoded_refresh_token = jwt.decode(
        refresh_token,
        env("DJANGO_SECRET_KEY"),
        algorithms=["HS256"],
        options={"verify_signature": False},
    )

    assert decoded_access_token["token_type"] == "access"
    assert decoded_access_token["user_id"] == create_user.pk

    assert decoded_refresh_token["token_type"] == "refresh"
    assert decoded_refresh_token["user_id"] == create_user.pk


def test_create_jwt_token__failure(api_client, create_user):
    data = {"username": create_user.username, "password": "failure_pass"}
    response = api_client.post(reverse("jwt-create"), data=data)

    assert response.status_code == status.HTTP_401_UNAUTHORIZED


def test_create_jwt_token__access_empty_request(api_client):
    data = {}
    response = api_client.post(reverse("jwt-create"), data=data)

    assert response.status_code == status.HTTP_400_BAD_REQUEST
