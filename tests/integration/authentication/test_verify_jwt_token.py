import pytest
from django.urls import reverse
from rest_framework import status

pytestmark = pytest.mark.django_db


def get_path() -> str:
    return "http://localhost:8000/api/v1/user/jwt/verify/"


def test_verify_jwt_token(api_client, create_user):
    data = {"username": create_user.username, "password": "university_pass"}
    response = api_client.post(reverse("jwt-create"), data=data)

    assert response.status_code == status.HTTP_200_OK

    access_token = response.json()["access"]
    data = {"token": access_token}
    response = api_client.post(reverse("jwt-verify"), data=data)

    assert response.status_code == status.HTTP_200_OK


def test_refresh_jwt_token__failure(api_client):
    data = {"token": "none"}
    response = api_client.post(reverse("jwt-verify"), data=data)

    assert response.status_code == status.HTTP_401_UNAUTHORIZED


def test_refresh_jwt_token__access_empty_request(api_client):
    data = {}
    response = api_client.post(reverse("jwt-verify"), data=data)

    assert response.status_code == status.HTTP_400_BAD_REQUEST
