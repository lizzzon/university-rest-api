import jwt
import pytest
from django.urls import reverse
from rest_framework import status

from core.env import env

pytestmark = pytest.mark.django_db


def test_refresh_jwt_token(api_client, create_user):
    data = {"username": create_user.username, "password": "university_pass"}
    response = api_client.post(reverse("jwt-create"), data=data)

    assert response.status_code == status.HTTP_200_OK

    refresh_token = response.json()["refresh"]
    data = {"refresh": refresh_token}
    response = api_client.post(reverse("jwt-refresh"), data=data)

    assert response.status_code == status.HTTP_200_OK

    access_token = response.json()["access"]
    decoded_access_token = jwt.decode(
        access_token,
        env("DJANGO_SECRET_KEY"),
        algorithms=["HS256"],
        options={"verify_signature": False},
    )

    assert decoded_access_token["token_type"] == "access"
    assert decoded_access_token["user_id"] == create_user.pk


def test_refresh_jwt_token__failure(api_client):
    data = {"refresh": "none"}
    response = api_client.post(reverse("jwt-refresh"), data=data)

    assert response.status_code == status.HTTP_401_UNAUTHORIZED


def test_refresh_jwt_token__access_empty_request(api_client):
    data = {}
    response = api_client.post(reverse("jwt-refresh"), data=data)

    assert response.status_code == status.HTTP_400_BAD_REQUEST
